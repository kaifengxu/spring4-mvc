package com.kf.springmvc.chapter_4_2_2;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WebInitialize implements WebApplicationInitializer{

	public void onStartup(ServletContext servletConfig) throws ServletException {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(MyMvcConfig.class);
		context.setServletContext(servletConfig);
		Dynamic d =servletConfig.addServlet("dispatcher", new DispatcherServlet(context));
		d.addMapping("/");
		d.setLoadOnStartup(1);
	}
	
}
