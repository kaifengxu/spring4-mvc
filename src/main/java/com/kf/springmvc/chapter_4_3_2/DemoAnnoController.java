package com.kf.springmvc.chapter_4_3_2;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/anno")
public class DemoAnnoController {
	
	@RequestMapping(produces = "text/plain;charset=UTF-8")
	public @ResponseBody String index(HttpServletRequest request) {
		return "url : " + request.getRequestURL() + " can access";
	}
	
	@RequestMapping(value = "/pathvar/{str}",produces = "text/plain;charset=UTF-8")
	public @ResponseBody String demoPathvar(HttpServletRequest request, @PathVariable String str) {
		return "url : " + request.getRequestURL() + " can access, str ： " +str ;
	}
	
	@RequestMapping(value = "/requestParam",produces = "text/plain;charset=UTF-8")
	public @ResponseBody String demoRequestParam(HttpServletRequest request,Long id) {
		return "url : " + request.getRequestURL() + " can access, id ： " +id ;
	}
	
	@RequestMapping(value = {"/name1","/name2"},produces = "text/plain;charset=UTF-8")
	public @ResponseBody String remove(HttpServletRequest request,Long id) {
		return "url : " + request.getRequestURL() + " can access";
	}
	
	@RequestMapping(value = "/obj",produces = "application/json;charset=UTF-8")
	public @ResponseBody String passObj(HttpServletRequest request,DemoObj obj, Long id) {
		System.out.println("id : "+ id);
		return "url : " + request.getRequestURL() + " can access, obj id ： " +obj.getId() +", obj name : "+ obj.getName() ;
	}
}
