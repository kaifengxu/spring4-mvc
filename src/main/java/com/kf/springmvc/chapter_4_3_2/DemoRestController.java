package com.kf.springmvc.chapter_4_3_2;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class DemoRestController {
	@RequestMapping(value = "/getjson",produces = "application/json;charset=UTF-8")
	public DemoObj getjson(HttpServletRequest request,DemoObj obj, Long id) {
		System.out.println("id : "+ id);
		return new DemoObj(obj.getId(), obj.getName());
	}
	@RequestMapping(value = "/getxml",produces = "application/xml;charset=UTF-8")
	public DemoObj getxml(HttpServletRequest request,DemoObj obj, Long id) {
		System.out.println("id : "+ id);
		return new DemoObj(obj.getId(), obj.getName());
	}
	
	@RequestMapping("/hello1")
	public String hello() {
		System.out.println("");
		return "hello";
	}
}
