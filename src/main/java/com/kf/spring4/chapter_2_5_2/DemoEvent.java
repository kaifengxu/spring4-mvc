package com.kf.spring4.chapter_2_5_2;

import org.springframework.context.ApplicationEvent;

public class DemoEvent extends ApplicationEvent{

	private static final long serialVersionUID = 6086920344461887981L;
	private String msg;
	public DemoEvent(Object source, String msg) {
		super(source);
		this.msg = msg;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
