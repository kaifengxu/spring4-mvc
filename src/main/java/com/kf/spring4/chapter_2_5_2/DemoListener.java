package com.kf.spring4.chapter_2_5_2;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DemoListener implements ApplicationListener<DemoEvent>{

	public void onApplicationEvent(DemoEvent event) {
		String msg = event.getMsg();
		System.out.println(" 我接收到了 demoEvent 消息 ： "+msg);
	}

}
