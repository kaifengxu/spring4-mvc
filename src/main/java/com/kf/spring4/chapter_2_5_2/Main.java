package com.kf.spring4.chapter_2_5_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new  AnnotationConfigApplicationContext(EventConfig.class);
		DemoPublisher p = context.getBean(DemoPublisher.class);
		p.publish("发布成功否？");
		context.close();
	}
}
