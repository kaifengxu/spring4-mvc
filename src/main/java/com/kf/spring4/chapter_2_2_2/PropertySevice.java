package com.kf.spring4.chapter_2_2_2;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:com/kf/chapter_2_2_2/test.properties")
public class PropertySevice {
	@Value("I love you")
	private String normal;
	
	@Value("#{systemProperties['os.name']}")
	private String osName;
	
	@Value("#{ T(java.lang.Math).random() * 100.0 }")
	private double randomNumer;
	
	@Value("#{demoService.author }")
	private String fromAuthor;
	
	@Value("classpath:com/kf/chapter_2_2_2/test.txt")
	private Resource testFile;
	
	@Value("https://www.baidu.com/")
	private Resource testUrl;
	
	@Value("${book.name}")
	private String bookName;
	
	@Autowired
	private Environment environment;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigure() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	public void outputResource() {
		try {
			System.out.println(normal);
			System.out.println(osName);
			System.out.println(randomNumer);
			System.out.println(fromAuthor);
			System.out.println(IOUtils.toString(testFile.getInputStream(), "utf-8"));
			System.out.println(IOUtils.toString(testUrl.getInputStream(), "utf-8"));
			System.out.println(bookName);
			System.out.println(environment.getProperty("book.author"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
