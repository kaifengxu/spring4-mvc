package com.kf.spring4.chapter_2_2_2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kf.spring4.chapter_2_2_2")
public class ElConfig {
	
	
}
