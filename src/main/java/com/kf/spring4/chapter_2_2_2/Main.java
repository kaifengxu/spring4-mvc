package com.kf.spring4.chapter_2_2_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ElConfig.class);
		PropertySevice p = context.getBean(PropertySevice.class);
		p.outputResource();
		context.close();
	}
}
