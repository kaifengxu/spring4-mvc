package com.kf.spring4.chapter_2_4_2;

public class DemoBean {
	private String content;
	public DemoBean(String content) {
		super();
		this.content = content;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
