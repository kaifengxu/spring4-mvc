package com.kf.spring4.chapter_2_4_2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ProfileConfig {
	@Bean
	@Profile("dev")
	public DemoBean devDemoBean() {
		return new DemoBean("dev...");
	}
	
	@Bean
	@Profile("pro")
	public DemoBean proDemoBean() {
		return new DemoBean("pro...");
	}
}
