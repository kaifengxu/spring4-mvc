package com.kf.spring4.chapter_2_4_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.getEnvironment().setActiveProfiles("dev");
		context.register(ProfileConfig.class);
		context.refresh();
		
		DemoBean d = context.getBean(DemoBean.class);
		System.out.println(d.getContent());
		context.close();
	}
}
