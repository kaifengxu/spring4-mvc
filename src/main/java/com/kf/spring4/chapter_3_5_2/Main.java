package com.kf.spring4.chapter_3_5_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		DemoService d = context.getBean(DemoService.class);
		d.outputResult();
		context.close();
	}

}
