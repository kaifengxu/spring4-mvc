package com.kf.spring4.chapter_3_1_2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kf.spring4.chapter_3_1_2")
public class AwareConfig {
	
}
