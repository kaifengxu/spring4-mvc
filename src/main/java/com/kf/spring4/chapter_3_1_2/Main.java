package com.kf.spring4.chapter_3_1_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AwareConfig.class);
		AwareService a = context.getBean(AwareService.class);
		a.outputResult();
		context.close();
	}
}
