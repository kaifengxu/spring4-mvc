package com.kf.spring4.interface_;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		InterfaceServices i = context.getBean(InterfaceServices.class);
		i.test();
		context.close();
	}
}
