package com.kf.spring4.interface_;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kf.spring4.interface_")
public class Config {

}
