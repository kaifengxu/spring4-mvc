package com.kf.spring4.interface_;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class InterfaceServices {
	
	@Autowired
	@Qualifier("interfaceImpl2")  
	InterfaceService interfaceService;
	public void test() {
		interfaceService.test();
	}
}
