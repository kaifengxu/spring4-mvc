package com.kf.spring4.chapter_1_3_3;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.kf.spring4.chapter_1_3_3")
@EnableAspectJAutoProxy
public class AopConfig {

}
