package com.kf.spring4.chapter_1_3_3;

import org.springframework.stereotype.Service;

@Service
public class DemoAnnotationService {
	@Action(name = "注解拦截器add操作")
	public void add() {}
}
