package com.kf.spring4.chapter_1_3_3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);
		DemoAnnotationService demoAnnotationService = context.getBean(DemoAnnotationService.class);
		demoAnnotationService.add();
		DemoMethodService demoMethodService = context.getBean(DemoMethodService.class);
		demoMethodService.add();
		context.close();
	}
}
