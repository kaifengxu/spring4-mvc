package com.kf.spring4.chapter_1_3_3;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoginAspect {
	@Pointcut("@annotation(Action)")
	public void annotationPointCut() {
		
	}
	
	@After("annotationPointCut()")
	public void after(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		Action action = method.getAnnotation(Action.class);
		System.out.println("@After 注解拦截 " + action.name());
	}
	
	@Before("annotationPointCut()")
	public void after1(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		Action action = method.getAnnotation(Action.class);
		System.out.println("@Before 注解拦截 " + action.name());
	}
	
	@After("execution(* com.kf.chapter_1_3_3.DemoMethodService.*(..))")
	public void before1(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		System.out.println("@After 规则拦截 " + method.getName());
	}
	
	@Before("execution(* com.kf.chapter_1_3_3.DemoMethodService.*(..))")
	public void before(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		System.out.println("@Before 规则拦截 " + method.getName());
	}
}
