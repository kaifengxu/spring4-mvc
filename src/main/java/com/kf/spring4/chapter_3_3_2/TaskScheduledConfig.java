package com.kf.spring4.chapter_3_3_2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
@Configuration
@ComponentScan("com.kf.spring4.chapter_3_3_2")
@EnableScheduling
public class TaskScheduledConfig implements AsyncConfigurer{
	
}
