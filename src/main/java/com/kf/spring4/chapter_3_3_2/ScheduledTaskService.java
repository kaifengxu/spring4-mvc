package com.kf.spring4.chapter_3_3_2;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduledTaskService {
	private static 	final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() {
		System.out.println("每隔5秒执行一次  " + dateFormat.format(new Date()));
	}
	
	@Scheduled(cron = "0 59 12 ? * *")
	public void reportCurrentTime1() {
		System.out.println("指定时间执行  " + dateFormat.format(new Date()));
	}
}
