package com.kf.spring4.chapter_3_2_2;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
@Configuration
@ComponentScan("com.kf.spring4.chapter_3_2_2")
@EnableAsync
public class TaskExecutorConfig implements AsyncConfigurer{
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor taskExecotor = new ThreadPoolTaskExecutor();
		taskExecotor.setCorePoolSize(5);
		taskExecotor.setMaxPoolSize(10);
		taskExecotor.setQueueCapacity(20);
		taskExecotor.initialize();
		return taskExecotor;
	}
}
