package com.kf.spring4.chapter_3_2_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TaskExecutorConfig.class);
		AsyncTaskService a = context.getBean(AsyncTaskService.class);
		for (int i = 0; i < 10; i++) {
			a.executeAsyncTask(i);
			a.executeAsyncTaskPlus(i);
		}
		context.close();
	}
}
