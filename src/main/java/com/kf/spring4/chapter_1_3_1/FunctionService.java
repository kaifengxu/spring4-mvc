package com.kf.spring4.chapter_1_3_1;

import org.springframework.stereotype.Service;

@Service
public class FunctionService {
	public String sayHello(String word) {
		return "helle " + word + " !";
	}
}
