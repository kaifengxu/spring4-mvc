package com.kf.spring4.chapter_1_3_1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kf.spring4.chapter_1_3_1")
public class DiConfig {

}
