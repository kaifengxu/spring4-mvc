package com.kf.spring4.chapter_1_3_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UseFunctionService {
	@Autowired
	FunctionService functionService;
	public String sayHello(String word) {
		return functionService.sayHello(word);
	}
}
