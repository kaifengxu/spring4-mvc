package com.kf.spring4.chapter_2_1_2;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class DemoSingletonService {

}
