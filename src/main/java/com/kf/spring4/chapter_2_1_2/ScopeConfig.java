package com.kf.spring4.chapter_2_1_2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kf.spring4.chapter_2_1_2")
public class ScopeConfig {

}
