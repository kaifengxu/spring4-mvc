package com.kf.spring4.chapter_2_3_2;

public class BeanWayService {
	public BeanWayService() {
		super();
		System.out.println("初始化构造函数 -- BeanWayService");
	}
	
	public void init() {
		System.out.println("@Bean - init");
	}
	

	public void destroy() {
		System.out.println("@Bean - destroy");
	}
}
