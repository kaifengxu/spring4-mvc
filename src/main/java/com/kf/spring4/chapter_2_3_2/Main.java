package com.kf.spring4.chapter_2_3_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(PrePostConfig.class);
		context.getBean(BeanWayService.class);
		context.getBean(Jsr250WayService.class);
		context.close();
	}
}
