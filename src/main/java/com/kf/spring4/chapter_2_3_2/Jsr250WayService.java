package com.kf.spring4.chapter_2_3_2;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Jsr250WayService {
	public Jsr250WayService() {
		super();
		System.out.println("初始化构造函数 -- Jsr250WayService");
	}
	
	@PostConstruct
	public void init() {
		System.out.println("Jsr250 - init");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("Jsr250 - destroy");
	}
}
