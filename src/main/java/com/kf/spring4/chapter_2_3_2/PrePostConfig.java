package com.kf.spring4.chapter_2_3_2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kf.spring4.chapter_2_3_2")
public class PrePostConfig {
	@Bean(initMethod="init", destroyMethod="destroy")
	BeanWayService beanWayService() {
		return new BeanWayService();
	}
	
	@Bean
	Jsr250WayService jsr250WayService() {
		return new Jsr250WayService();
		
	}
}
