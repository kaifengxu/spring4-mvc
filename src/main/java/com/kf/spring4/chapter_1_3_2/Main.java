package com.kf.spring4.chapter_1_3_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig.class);
		UseFunctionService us = context.getBean(UseFunctionService.class);
		System.out.println(us.sayHello("xkf_java"));
		context.close();
	}
}
