package com.kf.spring4.chapter_1_3_2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaConfig {
	
	@Bean
	public FunctionService functionService() {
		return new FunctionService();
	}
	
	@Bean
	public UseFunctionService useFunctionService() {
		UseFunctionService us = new UseFunctionService();
		us.setFunctionService(functionService());
		return us;
	}
	@Bean
	public UseFunctionService useFunctionService(FunctionService functionService) {
		UseFunctionService us = new UseFunctionService();
		us.setFunctionService(functionService);
		return us;
	}
}
