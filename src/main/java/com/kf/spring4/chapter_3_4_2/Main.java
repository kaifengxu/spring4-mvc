package com.kf.spring4.chapter_3_4_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConditionConfig.class);
		ListService l = context.getBean(ListService.class);
		System.out.println(context.getEnvironment().getProperty("os.name") + " 系统下的列表命令为 ：" + l.showListCmd());
		context.close();
	}
}	
