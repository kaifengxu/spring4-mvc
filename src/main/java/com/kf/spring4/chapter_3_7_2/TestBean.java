package com.kf.spring4.chapter_3_7_2;

public class TestBean {
	private String content;
	public TestBean(String content) {
		super();
		this.content = content;
	} 
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
