package com.kf.spring4.chapter_3_7_2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class TestConfig {
	@Bean
	@Profile("dev")
	public TestBean devDemoBean() {
		return new TestBean("dev...");
	}
	
	@Bean
	@Profile("pro")
	public TestBean proDemoBean() {
		return new TestBean("pro...");
	}
}
