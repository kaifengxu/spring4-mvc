package com.kf.chapter_3_7_2;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.kf.spring4.chapter_3_7_2.TestBean;

public class DemoBeanIntegrationTests {
	@Autowired
	private TestBean testBean;
	
	@Test
	public void prodBean() {
		System.out.println(testBean.getContent());
	}
}
